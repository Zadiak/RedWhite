class Product {
        constructor() {
            let data = new Controller().getValues();
            this.valueOne = Number(data.valueOne);
            this.valueTwo = Number(data.valueTwo);
            this.action = data.action;
        }
        calculate() {        
            switch (this.action) {
                case '+' :
                    this.sum();
                    break;
                case '-' :
                    this.min();
                    break;
                default :
                    this.result = 'Ошибка. Неизвестный оператор';
            }
            return this.result;
        }
        sum () {
            this.result = this.valueOne + this.valueTwo;
        }
        min () {
            this.result = this.valueOne - this.valueTwo;
        }
};