var catalog = {
    'beer' : [
        {
            'name' : "Essa",
            'Alcohol' : "7,5",
            'volume' : "0,5",
            'price' : "66", 
        },
        {
            'name' : "Clinskoe",
            'Alcohol' : "5",
            'volume' : "0,5",
            'price' : "47", 
        }
    ],
    'vodka' : [
        {
            'name' : "Morosha",
            'Alcohol' : "40",
            'volume' : "0,5",
            'price' : "320", 
        },
        {
            'name' : "Russian Standart",
            'Alcohol' : "40",
            'volume' : "0,7",
            'price' : "450", 
        }
    ],
    
    'konyk' : [
        {
            'name' : "Mor",
            'Alcohol' : "40",
            'volume' : "0,5",
            'price' : "320", 
        },
        {
            'name' : "RS",
            'Alcohol' : "40",
            'volume' : "0,7",
            'price' : "450", 
        }
    ]
};

var cat = new Catalog();
cat.setProducts(catalog);
cat.viewProducts();


