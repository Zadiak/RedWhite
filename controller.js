class Controller {
    getValues () {
        return {
            'valueOne' : document.getElementById("valueOne").value,
            'valueTwo' : document.getElementById("valueTwo").value,
            'action' : this.getRadioValue()
        };                
    }
    getRadioValue() {
        let radios = document.getElementsByName('action');
        for (var i = 0, length = radios.length; i < length; i++) {
            if (radios[i].checked) {
                return radios[i].value;
                break;
            }
        }
    }
    setResultValue(value) {
        document.getElementById("result").value = value;
    }
}
